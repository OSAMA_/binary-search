package com.epam.de.dsa;

public class BinarySearch {
    public int find(int[] elements, int value) {
        int low = 0;
        int high = elements.length - 1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (elements[mid] == value) {
                return mid;
            } else if (elements[mid] < value) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return -1;
    }
}
