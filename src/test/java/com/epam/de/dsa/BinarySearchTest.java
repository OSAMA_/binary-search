package com.epam.de.dsa;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTest {

    BinarySearch binarySearch;

    @BeforeEach
    void setUp(){
        binarySearch = new BinarySearch();
    }

    @Test
    void findTest(){
        int[] arr = {10,20,30,40,50,60,70};
        assertEquals(2,binarySearch.find(arr,30));
        assertEquals(-1,binarySearch.find(arr,80));
    }

}